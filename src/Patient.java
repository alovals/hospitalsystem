import java.sql.Date;

/**
 * @(#) Patient.java
 */

public class Patient
{
	private String name;
	
	private String surname;
	
	private String IDcode;
	
	private Date dateOfBirth;
	
	private String incurance;
	
	private String folder;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getIDcode() {
		return IDcode;
	}

	public void setIDcode(String IDcode) {
		this.IDcode = IDcode;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getIncurance() {
		return incurance;
	}

	public void setIncurance(String incurance) {
		this.incurance = incurance;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "Patient{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", IDcode='" + IDcode + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", incurance='" + incurance + '\'' +
				", folderID='" + folder + '\'' +
				'}';
	}
}
