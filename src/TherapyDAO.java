import java.sql.*;
import java.util.ArrayList;

/**
 * @(#) TherapyDAO.java
 */

public class TherapyDAO
{
    static Integer THERAPY_ID = 0;

	public static ArrayList<ArrayList<Date>> getTherapyDatesOfPatient(String[] therapies) {
        ArrayList<Date> startDates = new ArrayList<>();
        ArrayList<Date> endDates = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        ArrayList<ArrayList<Date>> output = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            for (String t: therapies) {
                result = stmt.executeQuery(
                        "SELECT startdate,enddate FROM therapy WHERE therapyID = '"+t+"';");
                while(result.next()){
                    Date startDate = result.getDate("startdate");
                    Date endDate = result.getDate("enddate");
                    startDates.add(startDate);
                    endDates.add(endDate);
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        output.add(startDates);
        output.add(endDates);
        return output;
	}

    public static ArrayList<String> getAllDaySchedules() {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        ArrayList<String> output = new ArrayList<String>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT schedulelist FROM therapy WHERE type='day';");
            while(result.next()){
                output.add(result.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static ArrayList<String> getAllOvernightSchedules() {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        ArrayList<String> output = new ArrayList<String>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT schedulelist FROM therapy WHERE type='overnight';");
            while(result.next()){
                output.add(result.getString(0));
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static Therapy getTherapy(String therapyId) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        Therapy output = new Therapy();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM therapy WHERE therapyID = '"+therapyId+"';");
            while(result.next()){
                Therapy t = new Therapy();
                t.setType(result.getString("type"));
                t.setMedicine(result.getString("medicineidlist"));
                t.setPosology(result.getString("posologylist"));
                t.setSchedule(result.getString("schedulelist"));
                t.setStartOfTherapy(result.getDate("startdate"));
                t.setEndOfTherapy(result.getDate("enddate"));
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public void insertDescription(String description )
	{
		
	}
	
	public void getFreeSpots( String days )
	{
		
	}
	
	public static void bookTherapyDays( String days )
	{
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("UPDATE therapy SET schedulelist = '"+days+"';");
            con.commit();
        }catch (Exception e) {
            e.printStackTrace(System.out);
        }
	}
	
	public static void insertTherapy(String visit_id, String patient_folder_id, String therapy_type, String medicine_list, String posology_list, Date start_date, String end_date )
	{
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO therapy VALUES ('"+(THERAPY_ID++)+"','"+therapy_type+"','"+medicine_list+"','"+posology_list+"','"+start_date+"','"+end_date+"','');");
            con.commit();
        }catch (Exception e) {
            e.printStackTrace(System.out);
        }
	}
	
	public static boolean getTherapyByID(String therapy_id )
	{
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM therapy WHERE therapyID = '"+therapy_id+"';");
            while(result.next()){
                output= true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }
	
	public static void dropPatientTherapies( String id )
	{
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            stmt.executeUpdate(
                    "DELETE FROM therapy WHERE therapyID = '"+id+"';");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
	}
	
}
