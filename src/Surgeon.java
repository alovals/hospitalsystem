/**
 * @(#) Surgeon.java
 */

public class Surgeon
{
	private String name;
	
	private String surname;
	
	private String professionalID;
	
	private SurgeonType surgeonType;
	
	private String surgery;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getProfessionalID() {
		return professionalID;
	}

	public void setProfessionalID(String professionalID) {
		this.professionalID = professionalID;
	}

	public SurgeonType getSurgeonType() {
		return surgeonType;
	}

	public void setSurgeonType(SurgeonType surgeonType) {
		this.surgeonType = surgeonType;
	}

	public String getSurgery() {
		return surgery;
	}

	public void setSurgery(String surgery) {
		this.surgery = surgery;
	}

	@Override
	public String toString() {
		return "Surgeon{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", professionalID='" + professionalID + '\'' +
				", surgeonType='" + surgeonType + '\'' +
				'}';
	}
}
