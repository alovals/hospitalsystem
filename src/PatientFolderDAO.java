import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @(#) PatientFolderDAO.java
 */

public class PatientFolderDAO
{
	public static void insertAnamnesis( String description, String patient_id)
	{
		Connection con = null;
		Statement stmt = null;
        System.out.println(patient_id);
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE patientfolder SET anamnesis = '"+description+"' WHERE folderID = '"+patient_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public void gettAllReports( String id_code )
	{
		
	}
	
	public void submitSoCalledFirstVisitForm( String filledForm )
	{
		
	}
	
	public void insertVisitReport( String report )
	{
		
	}
	
	public static boolean createPatientFolder( String patient_name, String surname, String patient_id, String birth_date, String insurance_code )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO patientfolder VALUES ('"+patient_name+"','"+surname+"','"+patient_id+"','"+birth_date+"','"+insurance_code+"');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void addTherapyReference( String therapy_id )
	{
		
	}
	public static List<String> getFolderByPatientID(String patient_id)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		List<String> output = new ArrayList<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM patientfolder WHERE patientID = '"+patient_id+"';");
			while(result.next()){
				for(int i = 1; i < 10; i++){
					output.add(result.getString(i));
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static List<String> getPatientFolderByID(String patient_folder_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		List<String> output = new ArrayList<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM patientfolder WHERE folderID = '"+patient_folder_id+"';");
			while(result.next()){
				for(int i = 1; i < 10; i++){
					output.add(result.getString(i));
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static void insertTestDescription( String patient_folder_id, String time, String date, String description )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("UPDATE patientfolder SET description = '"+description+"' WHERE patientID = '"+patient_folder_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static List<String> getAllPatientFolderIDs( )
	{
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<String> output = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT patientID FROM patientfolder;");
            while(result.next()){
                output.add(result.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
	}
	
	public static List<String> addTestReference(int test_id, String folder_id)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ResultSet resname = null;
		String testidList = null;
		String name = null;
		String surname = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery("SELECT testidlist FROM patientfolder WHERE folderID = '"+folder_id+"';");
			while(result.next()){
				if(result.getString(1).equals("")){
					testidList = Integer.toString(test_id);
				}else testidList = result.getString(1) +", "+ test_id;
			}
			stmt.executeUpdate("UPDATE patientfolder SET testidlist = '"+testidList+"' WHERE folderID = '"+folder_id+"';");
			resname = stmt.executeQuery("SELECT name,surname FROM patientfolder WHERE folderID = '"+folder_id+"';");
			while(resname.next()){
				name = resname.getString(1);
				surname = resname.getString(2);
			}
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return Arrays.asList(name, surname);
	}
	
	public static void addSurgeryReference(String surgery_id, String patient_id)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String surgeryidList = null;

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery("SELECT surgeryidlist FROM patientfolder WHERE patientID = '"+patient_id+"';");
			while(result.next()){
				if(result.getString(1).equals("")){
					surgeryidList = surgery_id;
				}else surgeryidList = result.getString(1) +", "+ surgery_id;
			}
			stmt.executeUpdate("UPDATE patientfolder SET surgeryidlist = '"+surgeryidList+"' WHERE patientID = '"+patient_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static HashMap<String,String> dropPatientFolder(String id )
	{
        Connection con = null;
        Statement stmt = null;
        ResultSet res1 = null;
        ResultSet res2 = null;
        ResultSet res3 = null;
        HashMap<String,String> info = new HashMap<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
            stmt = con.createStatement();
            res1 = stmt.executeQuery("SELECT testidlist FROM patientfolder WHERE patientID = '"+id+"';");
            while(res1.next()){
                info.put("testidlist", res1.getString(1));
            }
            res2 = stmt.executeQuery("SELECT therapyidlist FROM patientfolder WHERE patientID = '"+id+"';");
            while(res2.next()){
                info.put("therapyidlist", res2.getString(1));
            }
            res3 = stmt.executeQuery("SELECT surgeryidlist FROM patientfolder WHERE patientID = '"+id+"';");
            while(res3.next()){
                info.put("surgeryidlist", res3.getString(1));
            }
            stmt.executeUpdate(
                    "DELETE FROM patientfolder WHERE patientID = '"+id+"';");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return info;
	}
	
	public static void addFollowingOncologist( String patient_folder_id, String oncologist_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE patientfolder SET oncologistID = '"+oncologist_id+"' WHERE patientID = '"+patient_folder_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void addFirstVisitDate( String patient_folder_id, String date )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE patientfolder SET firstvisitdate = '"+date+"' WHERE patientID = '"+patient_folder_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static ArrayList<Patient> getAllPatients( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Patient> output = new ArrayList<Patient>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM patientfolder;");
			while(result.next()){
				Patient p = new Patient();
				p.setFolder(result.getString("folderID"));
				p.setName(result.getString("name"));
				p.setSurname(result.getString("surname"));
				p.setIDcode(result.getString("patientID"));
				p.setDateOfBirth(result.getDate("birthdate"));
				p.setIncurance(result.getString("code"));
				output.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static String getTherapyIDs(String patient_folder_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String output = "";
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT therapyidlist FROM patientfolder WHERE folderID = '"+patient_folder_id+"';");
			while(result.next()){
				output= result.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	
}
