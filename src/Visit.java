/**
 * @(#) Visit.java
 */

public class Visit
{
	private Integer dateOfVisit;
	
	private Patient patient;
	
	private Fellow fellow;
	
	private Specialist specialist;
	
	private Report report;
	
	private ImagingTest imagingTest;
	
	private Surgery surgery;
	
	private BloodTest bloodTest;
	
	private DayTimeTherapy dayTimeTherapy;
	
	private OverNightTherapy overNightTherapy;
	
	private HomeTherapy homeTherapy;
	
	private Integer time;
	
	
}
