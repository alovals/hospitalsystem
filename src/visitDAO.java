import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) visitDAO.java
 */

public class visitDAO
{
	static Integer VISIT_ID = 0;

	public static boolean checkIfBelongsToPatient(String visit_id, String patient_id) {
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM visit WHERE visitID = '"+visit_id+"' AND patientID = '"+patient_id+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}

	public static String getPatientIDFromVisit(String visitID) {
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String output = "";
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT patientID FROM visit WHERE visitID = '"+visitID+"';");
			while(result.next()){
				output = result.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}

	public static void addSurgeryReference(String surgery_id, String visitID)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String surgeryidList = null;

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery("SELECT surgeryidlist FROM visit WHERE visitID = '"+visitID+"';");
			while(result.next()){
				if(result.getString(1).equals("")){
					surgeryidList = surgery_id;
				}else surgeryidList = result.getString(1) +", "+ surgery_id;
			}
			stmt.executeUpdate("UPDATE visit SET surgeryidlist = '"+surgeryidList+"' WHERE visitID = '"+visitID+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void addVisitReport( String report, String visit_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String surgeryidList = null;

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE visit SET report = '"+report+"' WHERE visitID = '"+visit_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	public static List<String> getOncologistOnDay(String day) {
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		List<String> output = new ArrayList<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT oncologistID FROM visit WHERE date = '"+day+"';");
			while(result.next()){
				output.add(result.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}

	public void getBookedOncologists( String date )
	{
		
	}
	
	public static void bookVisit( String Patient_id, String date, String oncologist )
	{
		Connection con = null;
		Statement stmt = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("INSERT INTO visit VALUES ('"+(VISIT_ID++)+"','"+date+"','"+Patient_id+"','"+oncologist+"','','','','');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void addTestReference( int test_id, String visit_id)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String testidList = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery("SELECT testidlist FROM visit WHERE visitID = '"+visit_id+"';");
			while(result.next()){
				if(result.getString(1).equals("")){
					testidList = Integer.toString(test_id);
				}else testidList = result.getString(1) +", "+ test_id;
			}
			stmt.executeUpdate("UPDATE visit SET testidlist = '"+testidList+"' WHERE visitID = '"+visit_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public void addSoCalledFirstVisit( String patient_id )
	{
		
	}
	
	public void addTherapyReference( String therapy_id )
	{
		
	}
	
	public static boolean getVisitByID( String visit_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM visit WHERE visitID = '"+visit_id+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}

	public static List<String> getAllPatientReports(String patient_id){
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		List<String> output = new ArrayList<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT report FROM visit WHERE patientID = '"+patient_id+"';");
			while(result.next()){
				output.add(result.getString(1));
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}

	public static void dropPatientVisits(String patient_id){
		Connection con = null;
		Statement stmt = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate(
					"DELETE FROM visit WHERE patientID = '"+patient_id+"';");
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
}
