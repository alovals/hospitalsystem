/**
 * @(#) Medicine.java
 */

public class Medicine
{
	private String name;

	private String pharmaceuticalCompany;
	
	private String code;
	
	private String therapy;
	
	private String schedule;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPharmaceuticalCompany() {
		return pharmaceuticalCompany;
	}

	public void setPharmaceuticalCompany(String pharmaceuticalCompany) {
		this.pharmaceuticalCompany = pharmaceuticalCompany;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTherapy() {
		return therapy;
	}

	public void setTherapy(String therapy) {
		this.therapy = therapy;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	@Override
	public String toString() {
		return "Medicine{" +
				"name='" + name + '\'' +
				", pharmaceuticalCompany='" + pharmaceuticalCompany + '\'' +
				", code='" + code + '\'' +
				'}';
	}
}
