public enum OncologistType {
    RADIATION, MEDICAL, SURGICAL, GYNECOLOGIC, PEDIATRIC, HEMATOLOGIST
}
