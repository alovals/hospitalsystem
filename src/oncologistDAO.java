import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @(#) oncologistDAO.java
 */

public class oncologistDAO
{
	public static ArrayList<Oncologist> getOncologists( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Oncologist> output = new ArrayList<Oncologist>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM oncologist;");
			while(result.next()){
				Oncologist o = new Oncologist();
				o.setName(result.getString("name"));
				o.setSurname(result.getString("surname"));
				o.setProfessionalID(result.getString("professionalID"));
				o.setOncologistType(OncologistType.valueOf(result.getString("type").toUpperCase()));
				output.add(o);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public void getAllOncologistsIDs( )
	{
		
	}
	
	public static void addOncologist( String name, String surname, String id, String type, String level )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO oncologist VALUES ('"+id+"','"+name+"','"+surname+"','"+type+"', '"+level+"');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static boolean getOncologistByID(String id)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM oncologist WHERE professionalID = '"+id+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	
}
