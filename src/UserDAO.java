import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @(#) UserDAO.java
 */

public class UserDAO
{
	public static boolean checkUserNameAvailability( String userName )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = true;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM user WHERE username = '"+userName+"';");
			while(result.next()){
				output= false;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static void createUser( String userName, String password, String role )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO user VALUES ('"+userName+"','"+password+"','"+role+"');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static boolean existingUser(String UserName, String password)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM user WHERE username = '"+UserName+"' AND password = '"+password+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}


	public static String getRole(String userName) {
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		String output = "";
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT role FROM user WHERE username = '"+userName+"';");
			while(result.next()){
				output= result.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
}
