import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
/**
 * @(#) Boundary.java
 */

public class Boundary {

    //java -classpath lib/hsqldb.jar org.hsqldb.server.Server --database.0

    public static void main(String[] args) {
        System.out.println("Do you want to initialize the database? (Y/N)");
        Scanner scan = new Scanner(System.in);
        if (scan.nextLine().equals("Y")) {
            DatabaseManager.resetDatabase();
            DatabaseManager.initializeDatabase();
        }

        Controller controller = new Controller();

        while (true) {
            System.out.println("1. Register");
            System.out.println("2. Log in");
            System.out.println("3. Create patient folder");
            System.out.println("4. Add therapy");
            System.out.println("5. Add therapy schedule");
            System.out.println("6. Add anamnesis");
            System.out.println("7. Book visit");
            System.out.println("8. View test report");
            System.out.println("9. Print test report");
            System.out.println("10. Define surgery team");
            System.out.println("11. Get patient folder");
            System.out.println("12. Add oncologist");
            System.out.println("13. Write visit report");
            System.out.println("14. View medicines");
            System.out.println("15. Add medicine");
            System.out.println("16. Book test");
            System.out.println("17. Book surgery");
            System.out.println("18. Add surgeon");
            System.out.println("19. Delete patient folder");
            System.out.println("20. View surgeons");
            System.out.println("21. Assign oncologist");
            System.out.println("22. View oncologists");
            System.out.println("23. Set first visit date");
            System.out.println("24. View patients");
            System.out.println("25. Exit");
            //Adding old test is a functionality of a separate system

            int choice = Integer.valueOf(scan.nextLine());
            switch (choice) {
                case 1:
                    String startMessage = controller.startAccountRegisteringProcess();
                    System.out.println(startMessage);
                    System.out.println("Insert username: ");
                    int result = controller.insertUserName(scan.nextLine());
                    while (result != 0) {
                        switch (result) {
                            case 1:
                                System.out.println("Entered username is not valid. Must be 5-16 characters long, can not contain two subsequent \"__\" nor \"..\". Try again: ");
                                break;
                            case 2:
                                System.out.println("Entered username is taken. Try again: ");
                        }
                        result = controller.insertUserName(scan.nextLine());
                    }
                    System.out.println("Insert password: ");
                    boolean resultPass = controller.insertPassword(scan.nextLine());
                    while (!resultPass) {
                        System.out.println("Entered password is not valid. Must be 4-15 characters long, contain atleast one lowercase letter, one uppercase letter and one number. Try again: ");
                        resultPass = controller.insertPassword(scan.nextLine());
                    }
                    System.out.println("Insert role (“doctor”, “receptionist”, “administrative officer” or “surgeon”): ");
                    boolean resultRole = controller.insertRole(scan.nextLine());
                    while (!resultRole) {
                        System.out.println("Entered role is not valid (must be “doctor”, “receptionist”, “administrative officer” or “surgeon”). Try again: ");
                        resultRole = controller.insertRole(scan.nextLine());
                    }
                    System.out.println("Account successfully registered");
                    break;
                case 2:
                    String message = controller.startLogin();
                    System.out.println(message);
                    System.out.println("Insert username: ");
                    String username = scan.nextLine();
                    System.out.println("Insert password");
                    String password = scan.nextLine();
                    boolean login = controller.login(username, password);
                    while (!login) {
                        System.out.println("Invalid username or password. Try again: ");
                        System.out.println("Insert username: ");
                        username = scan.nextLine();
                        System.out.println("Insert password");
                        password = scan.nextLine();
                        login = controller.login(username, password);
                    }
                    System.out.println("Login successful");
                    break;
                case 3:
                    if (!controller.logged_in.equals("receptionist"))
                        System.out.println("Only receptionists can create new patient folders");
                    else {
                        System.out.println(controller.startFolderCreationProcess());
                        System.out.println("Enter the patient's name: ");
                        boolean validName = controller.insertName(scan.nextLine());
                        while (!validName) {
                            System.out.println("Entered name is not valid. Try again: ");
                            validName = controller.insertSurname(scan.nextLine());
                        }
                        System.out.println("Enter the patient's surname: ");
                        boolean validSurName = controller.insertSurname(scan.nextLine());
                        while (!validSurName) {
                            System.out.println("Entered surname is not valid. Try again: ");
                            validSurName = controller.insertSurname(scan.nextLine());
                        }
                        System.out.println("Enter the patient's ID: ");
                        boolean validID = controller.insertID(scan.nextLine());
                        while (!validID) {
                            System.out.println("Entered ID is not valid. Try again: ");
                            validID = controller.insertID(scan.nextLine());
                        }
                        System.out.println("Enter the patient's date of birth: ");
                        boolean validbirth = controller.insertPatientDateOfBirth(scan.nextLine());
                        while (!validbirth) {
                            System.out.println("Entered date of birth is not valid. Try again: ");
                            validbirth = controller.insertPatientDateOfBirth(scan.nextLine());
                        }
                        System.out.println("Enter the patient's private insurance code (if has private insurance code) or press ENTER: ");
                        int validInsurance = controller.insertInsuranceCode(scan.nextLine());
                        while (validInsurance == -1) {
                            System.out.println("Entered private insurance code is not valid. Try again: ");
                            validInsurance = controller.insertInsuranceCode(scan.nextLine());
                        }
                        if (validInsurance == 0)
                            System.out.println("Patient folder successfully created");
                        else
                            System.out.println("The patient folder already exists");
                    }
                    break;
                case 4:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can add therapies.");
                    else {
                        System.out.println("Insert the visit ID: ");
                        String visitID = scan.nextLine();
                        System.out.println("Insert patient ID: ");
                        String patientid = scan.nextLine();
                        System.out.println("Insert therapy type (day, home, overnight): ");
                        String type = scan.nextLine();
                        boolean isValidInput = controller.startAddingTherapy(visitID, patientid, type);
                        while (!isValidInput) {
                            System.out.println("Invalid input. Try again.");
                            System.out.println("Insert the visit ID: ");
                            visitID = scan.nextLine();
                            System.out.println("Insert patient ID: ");
                            patientid = scan.nextLine();
                            System.out.println("Insert therapy type (day, home, overnight):");
                            type = scan.nextLine();
                            isValidInput = controller.startAddingTherapy(visitID, patientid, type);
                        }
                        System.out.println("Please enter a list of medicine identifiers to be used (comma seprated, e.g 12313,42141,412414): ");
                        int validateMedicines = controller.insertMedicineList(scan.nextLine());
                        while (validateMedicines != 0) {
                            switch (validateMedicines) {
                                case 1:
                                    System.out.println("The list contains medicines that do not exist in the system. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The list contains duplicate medicines. Try again: ");
                            }
                            validateMedicines = controller.insertMedicineList(scan.nextLine());
                        }
                        System.out.println("Please enter a list of posologies for the corresponding medicines entered in the last step (in the same order): ");
                        int validatePosologies = controller.insertPosologyList(scan.nextLine());
                        while (validatePosologies != 0) {
                            switch (validatePosologies) {
                                case 1:
                                    System.out.println("The list contains more or less posologies than there are medicines. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The input is invalid. Try again: ");
                            }
                            validatePosologies = controller.insertPosologyList(scan.nextLine());
                        }
                        System.out.println("Enter a start date for the therapy: ");
                        int validateStartDate = controller.insertStartDate(scan.nextLine(), true);
                        while (validateStartDate != 0) {
                            switch (validateStartDate) {
                                case 1:
                                    System.out.println("The input is invalid. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The start date overlaps with an existing therapy for the same patient. Try again: ");
                            }
                            validateStartDate = controller.insertStartDate(scan.nextLine(), false);
                        }
                        System.out.println("Enter an end date for the therapy: ");
                        int validateEndDate = controller.insertEndDate(scan.nextLine());
                        while (validateEndDate != 0) {
                            switch (validateEndDate) {
                                case 1:
                                    System.out.println("The input is invalid. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The end date overlaps with an existing therapy for the same patient. Try again: ");
                                case 3:
                                    System.out.println("The end date cannot be before the start date! Try again:");
                            }
                            validateEndDate = controller.insertEndDate(scan.nextLine());
                        }
                        System.out.println("Therapy successfully added");
                    }
                    break;
                case 5:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can add therapy schedules.");
                    else {
                        System.out.println("Enter the therapy ID: ");
                        String therapyID = scan.nextLine();
                        System.out.println("Enter the visit ID: ");
                        String visID = scan.nextLine();
                        boolean validInputParams = controller.addTherapySchedule(therapyID, visID);
                        while (!validInputParams) {
                            System.out.println("Invalid identificator(s) entered. Try again: ");
                            validInputParams = controller.addTherapySchedule(therapyID, visID);
                        }
                        System.out.println("Enter the days of the therapy (as a list of comma separated values, e.g 2018-11-10,2018-11-11) to see the free spots: ");

                        ArrayList<String> validateDays = controller.insertTherapyDays(scan.nextLine());
                        while (validateDays.size() == 0 || validateDays.get(0).equals("error")) {
                            if (validateDays.size() == 0)
                                System.out.println("There are no free spots on the specified days. Try again: ");
                            else
                                System.out.println("The input is incorrect. Try again: ");
                            validateDays = controller.insertTherapyDays(scan.nextLine());
                        }

                        System.out.println("Choose the suitable days from the displayed list. Enter the desired days as a comma separated list (e.g 2018-11-10,2018-11-11): ");
                        int validateList = controller.chooseSuitableSpots(scan.nextLine());
                        while (validateList != 0) {
                            switch (validateList) {
                                case 1:
                                    System.out.println("There are no free spots on the specified days or the entered days were not chosen from the list. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The input is incorrect. Try again: ");
                                    break;
                                case 3:
                                    System.out.println("The list contains days that are not between the start date and the end date of the therapy. Try again: ");
                            }
                            validateList = controller.chooseSuitableSpots(scan.nextLine());
                        }
                        System.out.println("Therapy schedule successfully added!");
                    }
                    break;
                case 6:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can add anamnesis.");
                    else {
                        System.out.println("Enter the patient folder ID where the anamnesis must be added: ");
                        boolean validPatientFolderID = controller.addAnamnesis(scan.nextLine());
                        while (!validPatientFolderID) {
                            System.out.println("Entered patient folder ID is not valid. Try again: ");
                            validPatientFolderID = controller.addAnamnesis(scan.nextLine());
                        }
                        System.out.println("Enter the patient's anamnesis: ");
                        boolean isAnamnesisValid = controller.insertAnamnesis(scan.nextLine());
                        while (!isAnamnesisValid) {
                            System.out.println("The input cannot be empty. Try again: ");
                            isAnamnesisValid = controller.insertAnamnesis(scan.nextLine());
                        }
                    }
                    break;
                case 7:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can book visits.");
                    else {
                        System.out.println("Enter the patient ID: ");
                        boolean isValidPatientFolderID = controller.bookVisit(scan.nextLine());
                        while (!isValidPatientFolderID) {
                            System.out.println("Entered patient ID is not valid. Try again: ");
                            isValidPatientFolderID = controller.bookVisit(scan.nextLine());
                        }
                        System.out.println("Enter a day for when to book the visit: ");
                        int goodDay = controller.selectVisitDay(scan.nextLine());
                        while (goodDay != 0) {
                            switch (goodDay) {
                                case 1:
                                    System.out.println("There are no free oncologists on the specified day. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The input is incorrect. Try again: ");
                            }
                            goodDay = controller.selectVisitDay(scan.nextLine());
                        }
                        System.out.println("Enter an oncologist identifier to book him or her for the visit: ");
                        boolean isAvailable = controller.selectOncologist(scan.nextLine());
                        while (!isAvailable) {
                            System.out.println("Entered oncologist is not available. Try again: ");
                            isAvailable = controller.selectOncologist(scan.nextLine());
                        }
                        System.out.println("Visit booked successfully!");
                    }
                    break;
                case 8:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can view test reports.");
                    else {
                        System.out.println("Enter a patient ID: ");
                        int isValidPatientFolderID = controller.viewTestReport(scan.nextLine());
                        if(isValidPatientFolderID==2){
                            System.out.println("No reports to view");
                            break;
                        }
                        while (isValidPatientFolderID!=0) {
                            switch (isValidPatientFolderID){
                                case 1:
                                    System.out.println("Entered patient ID is not valid. Try again: ");
                                    break;
                            }
                            isValidPatientFolderID = controller.viewTestReport(scan.nextLine());
                        }
                        System.out.println("Enter a test report identifier (should be chosen from the list, Report0 or Report1 etc. For quiting type \"quit\"): ");
                        boolean isValidIdentifier = controller.selectReportIdentifierforViewing(scan.nextLine());
                        while (!isValidIdentifier) {
                            System.out.println("Entered report identifier is not valid. Try again: ");
                            isValidIdentifier = controller.selectReportIdentifierforViewing(scan.nextLine());
                        }
                    }
                    break;
                case 9:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can print test reports.");
                    else {
                        System.out.println("Enter the patient ID: ");
                        int isValidPatientFolderID = controller.viewTestReport(scan.nextLine());
                        if(isValidPatientFolderID==2){
                            System.out.println("No reports to print");
                            break;
                        }
                        while (isValidPatientFolderID!=0) {
                            switch (isValidPatientFolderID){
                                case 1:
                                    System.out.println("Entered patient ID is not valid. Try again: ");
                                    break;
                            }
                            isValidPatientFolderID = controller.viewTestReport(scan.nextLine());
                        }
                        System.out.println("Enter a test report identifier (should be chosen from the list, Report1, Report2 etc. For quiting type \"quit\"): ");
                        boolean isValidIdentifier = controller.selectReportIdentifier(scan.nextLine());
                        while (!isValidIdentifier) {
                            System.out.println("Entered report identifier is not valid. Try again: ");
                            isValidIdentifier = controller.selectReportIdentifier(scan.nextLine());
                        }
                    }
                    break;
                case 10:
                    if (!controller.logged_in.equals("surgeon"))
                        System.out.println("Only surgeons can define surgery teams.");
                    else {
                        System.out.println("Enter the surgery ID: ");
                        boolean isValidSurgeryID = controller.defineSurgeryTeam(scan.nextLine());
                        while (!isValidSurgeryID) {
                            System.out.println("Entered surgery ID does not exist. Try again: ");
                            isValidSurgeryID = controller.defineSurgeryTeam(scan.nextLine());
                        }
                        System.out.println("Enter the id codes of the team members as comma separated values (e.g 1213111,1231213,1444551): ");
                        int goodCodes = controller.insertTeamMembers(scan.nextLine());
                        while (goodCodes != 0) {
                            switch (goodCodes) {
                                case 1:
                                    System.out.println("The input format is incorrect. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("There is at least one incorrect ID code. Try again: ");
                            }
                            goodCodes = controller.insertTeamMembers(scan.nextLine());
                        }
                        System.out.println("Surgery team successfully defined");
                    }
                    break;
                case 11:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can get patient folders.");
                    else {
                        System.out.println("Enter the patient ID: ");
                        boolean isCorrect = controller.getPatientFolder(scan.nextLine());
                        while (!isCorrect) {
                            System.out.println("Entered patient ID is not valid. Try again: ");
                            isCorrect = controller.getPatientFolder(scan.nextLine());
                        }
                    }
                    break;
                case 12:
                    if (!controller.logged_in.equals("administrative officer"))
                        System.out.println("Only administrative officers can add oncologists.");
                    else {
                        System.out.println(controller.addOncologist());
                        System.out.println("Enter the name of the oncologist: ");
                        boolean enterName = controller.insertName(scan.nextLine());
                        while (!enterName) {
                            System.out.println("Entered name is not valid. Try again: ");
                            enterName = controller.insertName(scan.nextLine());
                        }
                        System.out.println("Enter the surname of the oncologist");
                        boolean surName = controller.insertSurname(scan.nextLine());
                        while (!surName) {
                            System.out.println("Entered surname is not valid. Try again: ");
                            surName = controller.insertSurname(scan.nextLine());
                        }
                        System.out.println("Enter the professional ID of the oncologist");
                        int profID = controller.insertIDOncologist(scan.nextLine());
                        while (profID != 0) {
                            switch (profID) {
                                case 1:
                                    System.out.println("Entered ID is not valid. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("Entered ID already exists. Try again: ");
                            }
                            profID = controller.insertIDOncologist(scan.nextLine());
                        }
                        System.out.println("Enter the type of the oncologist (RADIATION, MEDICAL, SURGICAL, GYNECOLOGIC, PEDIATRIC, HEMATOLOGIST): ");
                        boolean oncoType = controller.insertOncologistType(scan.nextLine());
                        while (!oncoType) {
                            System.out.println("Entered type is not valid. Try again: ");
                            oncoType = controller.insertOncologistType(scan.nextLine());
                        }
                        System.out.println("Enter the level of career of the oncologist (medical student, assistant, fellow, specialist)");
                        boolean level = controller.insertLevelOfCareer(scan.nextLine());
                        while (!level) {
                            System.out.println("Entered level of career is not valid. Try again: ");
                            level = controller.insertLevelOfCareer(scan.nextLine());
                        }
                        System.out.println("Oncologist successfully added!");
                    }
                    break;
                case 13:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can write visit reports.");
                    else {
                        System.out.println("Enter the visit ID: ");
                        boolean correctID = controller.writeVisitReport(scan.nextLine());
                        while (!correctID) {
                            System.out.println("Entered visit ID does not exist. Try again: ");
                            correctID = controller.writeVisitReport(scan.nextLine());
                        }
                        System.out.println("Enter the visit report: ");
                        boolean correctReport = controller.insertVisitReport(scan.nextLine());
                        while (!correctReport) {
                            System.out.println("Entered visit report cannot be empty. Try again: ");
                            correctReport = controller.insertVisitReport(scan.nextLine());
                        }
                    }
                    break;
                case 14:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can view medicines.");
                    else {
                        ArrayList<Medicine> medicinesView = controller.viewMedicines();
                        if (medicinesView.isEmpty())
                            System.out.println("There are no medicines in the system:");
                        else
                            for (Medicine m : medicinesView)
                                System.out.println(m);
                    }
                    break;
                case 15:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can add medicines.");
                    else {
                        System.out.println(controller.addMedicine());
                        System.out.println("Enter the name of the medicine: ");
                        boolean nameCorrect = controller.insertMedicineName(scan.nextLine());
                        while (!nameCorrect) {
                            System.out.println("Entered name is not valid. Try again: ");
                            nameCorrect = controller.insertMedicineName(scan.nextLine());
                        }
                        System.out.println("Enter the pharmaceutical company of the medicine: ");
                        boolean companyCorrect = controller.insertMedicineCompany(scan.nextLine());
                        while (!companyCorrect) {
                            System.out.println("Entered company is not valid. Try again: ");
                            companyCorrect = controller.insertMedicineCompany(scan.nextLine());
                        }
                        System.out.println("Enter the code of the medicine: ");
                        int goodCode = controller.insertMedicineCode(scan.nextLine());
                        while (goodCode != 0) {
                            switch (goodCode) {
                                case 1:
                                    System.out.println("Entered code is not valid. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("Entered code already exists in the system. Try again: ");
                            }
                            goodCode = controller.insertMedicineCode(scan.nextLine());
                        }
                        System.out.println("Medicine successfully added!");
                    }
                    break;
                case 16:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can book tests.");
                    else {
                        System.out.println("Enter the patient folder ID: ");
                        String patientFolderID = scan.nextLine();
                        System.out.println("Enter the visit ID: ");
                        String visitID = scan.nextLine();
                        int goodBooking = controller.bookTest(patientFolderID, visitID);
                        while (goodBooking != 0) {
                            switch (goodBooking) {
                                case 1:
                                    System.out.println("Entered patient folder ID is not valid. Try again: ");
                                    patientFolderID = scan.nextLine();
                                    break;
                                case 2:
                                    System.out.println("Entered visit ID is not valid. Try again: ");
                                    visitID = scan.nextLine();
                            }
                            goodBooking = controller.bookTest(patientFolderID, visitID);
                        }
                        System.out.println("Enter the desired test type ('X-Ray', 'PET', 'TC', 'MRI' or 'blood')");
                        boolean goodType = controller.inserTestType(scan.nextLine());
                        while (!goodType) {
                            System.out.println("Entered type is not valid. Try again: ");
                            goodType = controller.inserTestType(scan.nextLine());
                        }
                        System.out.println("Enter the day (yyyy-mm-dd) of the test: ");
                        int goodDay = controller.selectDay(scan.nextLine());
                        while (goodDay != 0) {
                            switch (goodDay) {
                                case 1:
                                    System.out.println("No available slots on the specified day. Try again (yyyy-mm-dd): ");
                                    break;
                                case 2:
                                    System.out.println("The input is incorrect. Try again (yyyy-mm-dd): ");
                            }
                            goodDay = controller.selectDay(scan.nextLine());
                        }
                        System.out.println("Enter a suitable test time (hh:mm)");
                        int goodTime = controller.insertTestTimeInt(scan.nextLine());
                        while (goodTime != 0) {
                            switch (goodTime) {
                                case 1:
                                    System.out.println("The specified time is not from the list of available slots. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The entered time is incorrect. Try again: ");
                            }
                            goodTime = controller.insertTestTimeInt(scan.nextLine());
                        }
                    }
                    break;
                case 17:
                    if (!controller.logged_in.equals("doctor"))
                        System.out.println("Only doctors can book surgeries.");
                    else {
                        System.out.println("Enter a visit ID to book a surgery: ");
                        boolean goodID = controller.bookSurgery(scan.nextLine());
                        while (!goodID) {
                            System.out.println("Entered visit ID is not valid. Try again: ");
                            goodID = controller.bookSurgery(scan.nextLine());
                        }
                        System.out.println("Enter the desired surgeon type ('general', 'pediatric', 'cardiothoracic','neurosurgery', 'oral', 'urology'): ");
                        int surgeonType = controller.insertSurgeonTypeforSurgery(scan.nextLine());
                        while (surgeonType != 0) {
                            switch (surgeonType) {
                                case 1:
                                    System.out.println("Currently zero surgeons of this type working. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("Entered surgeon type is not valid. Try again: ");
                                    break;
                            }
                            surgeonType = controller.insertSurgeonTypeforSurgery(scan.nextLine());
                        }
                        System.out.println("Enter a date (yyyy-mm-dd) when the surgery should be performed: ");
                        int goodDay = controller.chooseSurgeryDay(scan.nextLine());
                        while (goodDay != 0) {
                            switch (goodDay) {
                                case 1:
                                    System.out.println("No available surgeons of the specified type on the specified day. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The input is invalid. Try again: ");
                            }
                            goodDay = controller.chooseSurgeryDay(scan.nextLine());
                        }
                        System.out.println("Enter the ID code of the surgeon that will perform the surgery: ");
                        int goodSurgeon = controller.assignSurgeon(scan.nextLine());
                        while (goodSurgeon != 0) {
                            switch (goodSurgeon) {
                                case 1:
                                    System.out.println("The input is incorrect. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("The surgeon entered is not from the available surgeons list. Try again: ");
                            }
                            goodSurgeon = controller.assignSurgeon(scan.nextLine());
                        }
                    }
                    break;
                case 18:
                    if (!controller.logged_in.equals("administrative officer"))
                        System.out.println("Only administrative officers can add surgeons.");
                    else {
                        System.out.println(controller.addSurgeon());
                        System.out.println("Enter the name of the surgeon: ");
                        boolean enterName = controller.insertName(scan.nextLine());
                        while (!enterName) {
                            System.out.println("Entered name is not valid. Try again: ");
                            enterName = controller.insertName(scan.nextLine());
                        }
                        System.out.println("Enter the surname of the surgeon");
                        boolean surName = controller.insertSurname(scan.nextLine());
                        while (!surName) {
                            System.out.println("Entered surname is not valid. Try again: ");
                            surName = controller.insertSurname(scan.nextLine());
                        }
                        System.out.println("Enter the professional ID of the surgeon");
                        int profID = controller.insertIDSurgeon(scan.nextLine());
                        while (profID != 0) {
                            switch (profID) {
                                case 1:
                                    System.out.println("Entered ID is not valid. Try again: ");
                                    break;
                                case 2:
                                    System.out.println("Entered ID already exists. Try again: ");
                            }
                            profID = controller.insertIDSurgeon(scan.nextLine());
                        }
                        System.out.println("Enter the type of the surgeon (GENERAL, PEDIATRIC, CARDIOTHORACIC, NEUROSURGERY, ORAL, UROLOGY):");
                        boolean surgeonTypeadd = controller.insertSurgeonType(scan.nextLine());
                        while (!surgeonTypeadd) {
                            System.out.println("Entered type is not valid (GENERAL, PEDIATRIC, CARDIOTHORACIC, NEUROSURGERY, ORAL, UROLOGY). Try again: ");
                            surgeonTypeadd = controller.insertSurgeonType(scan.nextLine());
                        }
                        System.out.println("Surgeon successfully added!");
                    }
                    break;
                case 19:
                    if (!controller.logged_in.equals("administrative officer"))
                        System.out.println("Only administrative officers can delete patient folders.");
                    else {
                        System.out.println("Enter the patient ID whose folder should be deleted: ");
                        boolean validId = controller.dletePatientFolder(scan.nextLine());
                        while (!validId) {
                            System.out.println("Entered patient ID is not valid. Try again: ");
                            validId = controller.dletePatientFolder(scan.nextLine());
                        }
                    }
                    break;
                case 20:
                    if (!controller.logged_in.equals("administrative officer"))
                        System.out.println("Only administrative officer can view surgeons.");
                    else {
                        ArrayList<Surgeon> surgeons = controller.viewSurgeons();
                        if (surgeons.size() > 0) {
                            for (Surgeon s : surgeons) {
                                System.out.println(s);
                            }
                        } else
                            System.out.println("There are no surgeons in the system to display.");
                    }
                    break;
                case 21:
                    if (!controller.logged_in.equals("receptionist"))
                        System.out.println("Only receptionists can assign oncologists to follow a patient's case.");
                    else {
                        System.out.println("Enter the patient ID where an oncologist should be added");
                        boolean validId = controller.assignOncologist(scan.nextLine());
                        while (!validId) {
                            System.out.println("Entered patient ID does not exist. Try again: ");
                            validId = controller.assignOncologist(scan.nextLine());
                        }
                        System.out.println("Enter the oncologist's ID that needs to be assigned to follow a patient's case: ");
                        boolean oncoID = controller.insertID(scan.nextLine());
                        while (!oncoID) {
                            System.out.println("Entered oncologist ID is not valid. Try again: ");
                            oncoID = controller.insertID(scan.nextLine());
                        }
                        System.out.println("Oncologist successfully assigned to the patient.");
                    }
                    break;
                case 22:
                    if (!controller.logged_in.equals("administrative officer"))
                        System.out.println("Only administrative officers view oncologists.");
                    else {
                        ArrayList<Oncologist> oncologists = controller.viewOncologists();
                        if (oncologists.size() > 0) {
                            for (Oncologist o : oncologists) {
                                System.out.println(o);
                            }
                        } else
                            System.out.println("There are no oncologists in the system to display.");
                    }
                    break;
                case 23:
                    if (!controller.logged_in.equals("receptionist"))
                        System.out.println("Only receptionists can set first visit dates in a patient's folder.");
                    else {
                        System.out.println("Enter the patient ID to whose folder where the first visit date needs to be set: ");
                        boolean validId = controller.setFirstVisitDate(scan.nextLine());
                        while (!validId) {
                            System.out.println("Entered patient ID is not valid. Try again: ");
                            validId = controller.setFirstVisitDate(scan.nextLine());
                        }
                        System.out.println("Enter the first visit date (in the format YYYY-MM-DD): ");
                        boolean validDate = controller.insertDate(scan.nextLine());
                        while (!validDate) {
                            System.out.println("Entered date is not valid (must be in YYYY-MM-DD format). Try again: ");
                            validDate = controller.insertDate(scan.nextLine());
                        }
                        System.out.println("First visit date successfully added to the patient folder!");
                    }
                    break;
                case 24:
                    if (!controller.logged_in.equals("receptionist"))
                        System.out.println("Only receptionists can view existing patients.");
                    else {
                        ArrayList<Patient> patients = controller.viewPatients();
                        if (patients.size() > 0) {
                            for (Patient p : patients) {
                                System.out.println(p);
                            }
                        } else
                            System.out.println("There are no patients in the system to display.");
                    }

            }
            if (choice == 25) // Exit the system
                break;

        }
        scan.close();
    }
}

