import java.sql.Date;
import java.util.*;
import java.util.regex.*;

/**
 * @(#) Controller.java
 */

public class Controller
{
	String username;
	String password;
	String patienFolderId;
	String time;
	String oldTestDate;
	String description;
	String name;
	String surname;
	String patientID;
	String dateOfBirth;
	String insuranceCode;
	String visitID;
	String testType;
	String therapyType;
	List<String> availableTestSlots;
	String testDate;
	String medicineName;
	String medicineCompany;
	String medicineID;
	String bookingTestTime;
	List<String> patientFolder;
	String oncologistId;
	String oncologistName;
	String type;
	String levelOfCareer;
	String surgeonId;
	String surgeryId;
	String medicineList;
	String posologyList;
	String therapyId;
	Date therapyStartDate;
	int enteredMedicinesCount;
	String therapyDays;
	ArrayList<Date> startDates = new ArrayList<>();
	ArrayList<Date> endDates= new ArrayList<>();
	ArrayList<String> validDays;
	HashMap<String,String> patientReportIdentifiersAndReports = new HashMap<>();
	String surgeonType;
	List<String> availableSurgeonsOnDay = new ArrayList<>();
	String surgeryDate;
	List<String> freeOncologistOnDay;
	String visitDay;

	String logged_in = "";

	public boolean login(String userName, String password) {
		boolean userExists = UserDAO.existingUser(userName, password);
		if (userExists) {
			logged_in = UserDAO.getRole(userName).toLowerCase();
			return true;
		}
		return false;
	}

	public void checkIfUserNameAndPasswordValid(String userName, String password )
	{

	}
	
	public void checkIfFormIsValid1( String filledForm )
	{
		
	}
	
	public String addMedicine( )
	{
		return "Starting the medicine adding process";
	}
	
	public boolean insertMedicineName(String name )
	{
		boolean medicineNameIsValid = validateMedicineName(name);
		if (medicineNameIsValid) {
			this.medicineName = name;
			return true;
		}
		return false;
	}
	
	public boolean validateMedicineName(String name )
	{

		return true;
	}
	
	public ArrayList<Medicine> viewMedicines( )
	{
		return MedicineDAO.getAllNames();
	}
	
	public void viewSpecificMedicine( String medicine )
	{
		
	}
	
	public boolean insertDescription( String description )
	{
		boolean descriptionIsValid = validateTestDescription(description);
		if(descriptionIsValid){
			this.description = description;
			PatientFolderDAO.insertTestDescription(this.patienFolderId, this.time, this.oldTestDate, this.description);
			return true;
		}
		return false;
	}
	
	public boolean addTherapySchedule(String therapy_id, String visit_id )
	{
		this.therapyId = therapy_id;
		boolean therapyIdExists = TherapyDAO.getTherapyByID(therapy_id);
		boolean visitIdExists = visitDAO.getVisitByID(visit_id);
		return therapyIdExists && visitIdExists;
	}
	
	public void chooseTherapy( String therapy )
	{
		
	}
	
	public ArrayList<String> insertTherapyDays(String days )
	{
		boolean validInput = validateInputList(days);
		if (!validInput)
			return new ArrayList<String>();
		else {
			validDays = new ArrayList<>();

			Map<String, Integer> scheduledDaysMapDay = new HashMap<String, Integer>();
			ArrayList<String> bookedDatesDay = TherapyDAO.getAllDaySchedules();
			for (String day: bookedDatesDay) {
				Integer bookedSpots = scheduledDaysMapDay.get(day);
				if (bookedSpots != null) {
					scheduledDaysMapDay.put(day, bookedSpots + 1);
				}
				else {
					scheduledDaysMapDay.put(day, 1);
				}
			}

			Map<String, Integer> scheduledDaysMapOvernight = new HashMap<String, Integer>();
			ArrayList<String> bookedDatesOvernight = TherapyDAO.getAllOvernightSchedules();
			for (String day: bookedDatesOvernight) {
				Integer bookedSpots = scheduledDaysMapOvernight.get(day);
				if (bookedSpots != null) {
					scheduledDaysMapOvernight.put(day, bookedSpots + 1);
				}
				else {
					scheduledDaysMapOvernight.put(day, 1);
				}
			}

			String[] daysArray = days.split(",");
			for (String day: daysArray) {
				if (therapyType == "day") {
					if (scheduledDaysMapDay.get(day) != null && scheduledDaysMapOvernight.get(day)!=null && scheduledDaysMapDay.get(day) < 10 && (scheduledDaysMapDay.get(day) + scheduledDaysMapOvernight.get(day)) < 20) {
						validDays.add(day);
						scheduledDaysMapDay.put(day, scheduledDaysMapDay.get(day) + 1);
					}
					else if (scheduledDaysMapDay.get(day)!= null && scheduledDaysMapDay.get(day) < 10 && scheduledDaysMapOvernight.get(day) == null) {
						validDays.add(day);
						scheduledDaysMapDay.put(day, scheduledDaysMapDay.get(day) + 1);
					}
					else if (scheduledDaysMapDay.get(day) == null && (scheduledDaysMapOvernight.get(day) == null || scheduledDaysMapOvernight.get(day) < 20)) {
						validDays.add(day);
						scheduledDaysMapDay.put(day, 1);
					}
				}
				else {
					if (scheduledDaysMapDay.get(day) != null && scheduledDaysMapOvernight.get(day)!=null && (scheduledDaysMapDay.get(day) + scheduledDaysMapOvernight.get(day)) < 20) {
						validDays.add(day);
						scheduledDaysMapOvernight.put(day, scheduledDaysMapOvernight.get(day) + 1);
					}
					else if (scheduledDaysMapOvernight.get(day)!= null && scheduledDaysMapOvernight.get(day) < 20 && scheduledDaysMapDay.get(day) == null) {
						validDays.add(day);
						scheduledDaysMapOvernight.put(day, scheduledDaysMapOvernight.get(day) + 1);
					}
					else if (scheduledDaysMapOvernight.get(day) == null && scheduledDaysMapDay.get(day) == null ) {
						validDays.add(day);
						scheduledDaysMapOvernight.put(day, 1);
					}
				}
			}

			if (validDays.size() == 0)  {
				ArrayList<String> strings = new ArrayList<>();
				strings.add("error");
			}
		}
        System.out.println(validDays);
		return validDays;
	}
	
	public int chooseSuitableSpots(String days )
	{
		boolean validInputList = validateInputList(days);
		if (!validInputList)
			return 2;
		String[] daysArray = days.split(",");
		for (String day: daysArray) {
			if (!validDays.contains(day))
				return 1;
		}

		Therapy t = TherapyDAO.getTherapy(therapyId);
		String schedule = t.getSchedule();
		Date startDateOfTherapy = t.getStartOfTherapy();
		Date endDateOfTherapy = t.getEndOfTherapy();
		//startDateOfTherapy.setYear(startDateOfTherapy.getYear());
        //startDateOfTherapy.setYear(startDateOfTherapy.getMonth()-1);

		for (String day: daysArray) {
			String parts[] = day.split("-");
			Integer yearNr = Integer.valueOf(parts[0])-1900;
			Integer monthNr = Integer.valueOf(parts[1])-1;
			Integer dayNr = Integer.valueOf(parts[2]);
			Date dateValue = new Date(yearNr, monthNr, dayNr);

			if (dateValue.compareTo(startDateOfTherapy) <= 0 || dateValue.compareTo(endDateOfTherapy) >= 0)
				return 3;
		}

		for (String d: daysArray) {
			if(schedule.length()==0)
				schedule += d;
			else
				schedule += "," + d;
		}
		TherapyDAO.bookTherapyDays(schedule);
		return 0;
	}
	
	public boolean addAnamnesis(String patient_folder_id )
	{
		List<String> patient_fol = PatientFolderDAO.getPatientFolderByID(patient_folder_id);
		if(patient_fol.size()!=0){
			this.patientFolder = patient_fol;
			return true;
		}
		return false;
	}
	
	public boolean insertAnamnesis(String description )
	{
		boolean isValidAnamnesis = validateAnamnesis(description);
		if(isValidAnamnesis){
			PatientFolderDAO.insertAnamnesis(description, this.patientFolder.get(0));
			System.out.println();
			System.out.println("Anamnesis added to patient folder successfully!");
			System.out.println("-------------------------------------------------");
			System.out.println();
			return true;
		}
		return false;
	}
	
	public boolean validateAnamnesis( String description )
	{
		return description.length()!=0;
	}
	
	public boolean addOldTest( String patient_folder_id )
	{
		patienFolderId = patient_folder_id;
		boolean validateID = PatientFolderDAO.getPatientFolderByID(patient_folder_id).size() > 0;
		return validateID;
	}
	
	public String viewTestReports( )
	{
		return "Started report viewing process";
	}
	
	public int viewTestReport(String patient_id )
	{
        List<String> patientids = PatientFolderDAO.getAllPatientFolderIDs();
        boolean isExisting = patientids.contains(patient_id);
        if(isExisting){
            List<String> reports = visitDAO.getAllPatientReports(patient_id);
            if(reports.size()!=0){
                HashMap<String, String> identifiersAndReeports = new HashMap<>();
                System.out.println();
                System.out.println("----------------------------------------");
                System.out.println("Patient report identifier and descriptions:");
                System.out.println("----------------------------------------");
                for (int i = 0; i < reports.size(); i++){
                    String repo = reports.get(i);
                    identifiersAndReeports.put("report"+i, repo);
                    int repolen = repo.length();
                    if(repolen>100){
                        System.out.println("Report"+i+": "+repo.substring(0,100)+". . .");
                    }
                    else System.out.println("Report"+i+": "+repo.substring(0,repolen)+". . .");

                }
                System.out.println("----------------------------------------");
                System.out.println();
                this.patientReportIdentifiersAndReports = identifiersAndReeports;
                return 0;
            }
            return 2;
        }
		return 1;
	}
	
	public String bookSurgery( )
	{
		return "Started surgery booking process";
	}
	
	public boolean bookSurgery(String id_code )
	{
        if(validateID(id_code)){
            boolean exists = visitDAO.getVisitByID(id_code);
            if(exists){
                this.visitID = id_code;
                return true;
            }
            return false;
        }
		return false;
	}
	
	public void chooseDayList( String days )
	{
		
	}
	
	public int chooseSurgeryDay(String day )
	{
        boolean isvalidDate = validateEnteredDate(day);
        if (isvalidDate){
            //need all surgeons ids of given type
            ArrayList<Surgeon> surgeonsList = SurgeonDAO.getAllSurgeons();
            List<String> surgeon_ids_of_given_type = new ArrayList<>();
            for (Surgeon surgeon : surgeonsList){
                if (surgeon.getSurgeonType()==SurgeonType.valueOf(this.surgeonType.toUpperCase())){
                    surgeon_ids_of_given_type.add(surgeon.getProfessionalID());
                }
            }
            //need all dates of surgeries which are associated to surgeons of given type
            HashMap<String,String> booked_surgeons_ondatestimes = SurgeryDAO.getBookedSurgeonsOnDay(day);
            Set<String> booked_surgeons = booked_surgeons_ondatestimes.keySet();


            //filter out free surgeons of given type
            for(String booked_surgeon : booked_surgeons){
                if(surgeon_ids_of_given_type.contains(booked_surgeon)){
                    surgeon_ids_of_given_type.remove(booked_surgeon);
                }
            }
            if(surgeon_ids_of_given_type.size()!=0){
                this.surgeryDate = day;
                System.out.println("--------------------------");
                System.out.println();
                System.out.println("Free surgeons ID list: ");
                System.out.println(surgeon_ids_of_given_type);
                System.out.println();
                System.out.println("--------------------------");
                this.availableSurgeonsOnDay = surgeon_ids_of_given_type;
                return 0;
            }
            return 1;
        }
		return 2;
	}
	
	public int assignSurgeon(String surgeon_id )
	{
        boolean isValid = validateID(surgeon_id);
        if(isValid){
            if(this.availableSurgeonsOnDay.contains(surgeon_id)){
                String patient_id = visitDAO.getPatientIDFromVisit(this.visitID);
                //add surgery
                String surgery_id = SurgeryDAO.addSurgery(surgeon_id, this.surgeryDate);
                PatientFolderDAO.addSurgeryReference(surgery_id, patient_id);
                visitDAO.addSurgeryReference(surgery_id, this.visitID);
                System.out.println();
                System.out.println("----------------------------------");
                System.out.println("Surgeon assigned to the surgery!");
                System.out.println("----------------------------------");
                System.out.println();
                return 0;
            }
            return 2;
        }
		return 1;
	}
	
	public void bookSurgery1( String visit_id )
	{
		
	}
	
	public String startAccountRegisteringProcess( )
	{
		return "Started account registration process";
	}
	
	public String startLogin( )
	{
		return "Started log-in process";
	}
	
	public String openForm( )
	{
		return "Started form opening process";
	}
	
	public void checkForm( String filledForm )
	{
		
	}
	
	public void checkIfFormIsValid( String filledForm )
	{
		
	}
	
	public boolean getPatientFolder(String patient_id )
	{
		List<String> folder = PatientFolderDAO.getFolderByPatientID(patient_id);
		if(folder.size()!=0){
			System.out.println("Patient folder info:");
			System.out.println("Folder ID: "+folder.get(0));
			System.out.println("Name: "+folder.get(1));
			System.out.println("Surname: "+folder.get(2));
			System.out.println("Patient ID: "+folder.get(3));
			System.out.println("Date of birth: "+folder.get(5));
			System.out.println("Insurance code: "+folder.get(6));
			System.out.println("-------------------------------------------------");
			return true;
		}
		return false;
	}
	
	public boolean writeVisitReport(String visit_id )
	{
		visitID = visit_id;
		boolean validVisitId = visitDAO.getVisitByID(visit_id);
		return validVisitId;
	}
	
	public boolean insertVisitReport(String report )
	{
		boolean validReport = checkVisitReport(report);
		if (!validReport)
			return false;
		visitDAO.addVisitReport(report, visitID);
		return true;
	}
	
	public boolean checkVisitReport(String report )
	{
		return report.length() != 0;
	}
	
	public boolean bookVisit(String patient_id )
	{
		boolean isValid = validateID(patient_id);
		patientID = patient_id;
		boolean exists = PatientFolderDAO.getFolderByPatientID(patient_id).size()!=0;
		if(isValid && exists)return true;
		return false;
	}
	
	public int selectDay( String date )
	{
		boolean dateIsValid = validateEnteredDate(date);
		if(dateIsValid){
			this.testDate = date;
			List<String> availableSlots = TestDAO.getAvailableTestSlots(this.testDate, this.testType);
			if(availableSlots.size()!=0){
				//to proper shape for actor
				for(int i = 0; i < availableSlots.size(); i++){
					String[] info = availableSlots.get(i).split(":");
					availableSlots.remove(i);
					if (info[0].length()==1){
						availableSlots.add(i, "0"+info[0]+":"+info[1]);
					}
					else availableSlots.add(i, info[0]+":"+info[1]);
				}
				this.availableTestSlots = availableSlots;
				System.out.println("Available slots on chosen day: ");
				System.out.println(availableSlots);
				return 0;
			}
			return 1;
		}
		return 2;
	}
	
	public boolean selectOncologist(String oncologist )
	{
		if(freeOncologistOnDay.contains(oncologist)){
			visitDAO.bookVisit(patientID, visitDay, oncologist);
			return true;
		}
		return false;
	}
	
	public int bookTest( String patient_folder_id, String visit_id )
	{
		this.visitID = visit_id;
		List<String> patientFolderExsts = PatientFolderDAO.getPatientFolderByID(patient_folder_id);
		if(patientFolderExsts.size()!=0){
			boolean visitExists = visitDAO.getVisitByID(this.visitID);
			if (visitExists) return 0;
			return 2;
		}
		return 1;
	}
	
	public String startBookingProcess( )
	{
		return "Started booking process";
	}
	
	public boolean insertID(String id )
	{
		oncologistId = id;
		boolean oncologistExists = oncologistDAO.getOncologistByID(id);
		if (oncologistExists) {
			PatientFolderDAO.addFollowingOncologist(patienFolderId, oncologistId);
			return true;
		}
		return false;
	}
	
	public String startFolderCreationProcess( )
	{
		return "Started folder creating process";
	}
	
	public boolean insertName( String name )
	{
		boolean nameIsValid = validateName(name);
		if(nameIsValid){
			this.name = name;
			return true;
		}
		return false;
	}
	
	public boolean validateName( String name )
	{
		Pattern pattern = Pattern.compile("[A-Z][a-zA-Z]*");
		Matcher matcher = pattern.matcher(name);
		boolean validName = matcher.matches();
		if(validName) return true;
		return false;
	}
	
	public boolean insertSurname( String surname )
	{
		boolean surNameIsValid = validateSurname(surname);
		if(surNameIsValid){
			this.surname = surname;
			return true;
		}
		return false;
	}
	
	public boolean validateSurname( String surname )
	{
		Pattern pattern = Pattern.compile("[a-zA-z]+([ '-][a-zA-Z]+)*");
		Matcher matcher = pattern.matcher(surname);
		boolean validSurname = matcher.matches();
		if(validSurname) return true;
		return false;
	}

	public boolean insertPatientID(String patient_id)
	{
		boolean idIsValid = validateID(patient_id);
		if(idIsValid){
			this.patientID = patient_id;
			return true;
		}
		return false;
	}

	public boolean validateID(String id )
	{
		Pattern pattern = Pattern.compile("^\\w*\\d*$");
		Matcher matcher = pattern.matcher(id);
		boolean validID = matcher.matches();
		if(validID) return true;
		return false;
	}
	
	public boolean insertPatientDateOfBirth( String birth_date )
	{
		boolean bdateIsValid = validateDateOfBirth(birth_date);
		if(bdateIsValid){
			this.dateOfBirth = birth_date;
			return true;
		}
		return false;
	}
	
	public boolean validateDateOfBirth( String birth_date )
	{
		Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
		Matcher matcher = pattern.matcher(birth_date);
		boolean validBirthDate = matcher.matches();
		if(validBirthDate) return true;
		return false;
	}
	
	public int insertInsuranceCode( String insurance_code )
	{
		// -1(not valid code), 0(okay, success), 1(okay but already exists)
		boolean insuranceCodeIsValid = validateInsuranceCode(insurance_code);
		if(insuranceCodeIsValid){
			boolean canBeUsed = PatientFolderDAO.createPatientFolder(this.name, this.surname, this.patientID, this.dateOfBirth, this.insuranceCode);
			if(canBeUsed)return 0;
			return 1;
		}
		return -1;
	}
	
	public boolean validateInsuranceCode( String insuranceCode )
	{
		return ((this.surname+this.patientID).equals(insuranceCode) || insuranceCode.equals(""));
	}
	
	public boolean startAddingTherapy(String visit_id, String patient_folder_id, String therapy_type )
	{
		this.visitID = visit_id;
		this.patienFolderId = patient_folder_id;
		this.therapyType = therapy_type;
		boolean informationIsValid = validateInformation(visit_id, patient_folder_id, therapy_type);
		return informationIsValid;
	}
	
	public boolean validateInformation(String visit_id, String patient_id, String therapy_type )
	{
		boolean existsPatientIdWithVisit = visitDAO.checkIfBelongsToPatient(visit_id, patient_id);
		if (existsPatientIdWithVisit) {
			if (therapy_type.toLowerCase().equals("home") || therapy_type.toLowerCase().equals("day") || therapy_type.toLowerCase().equals("overnight")){
				return true;
			}
			return false;
		}
		return false;
	}
	
	public boolean validateInputList(String days )
	{
		therapyDays = days;
		String[] daysArray = days.split(",");
		for (String day: daysArray){
			if (!validateEnteredDate(day))
				return false;
		}
		return true;
	}

	// YYYY-MM-DD
	public boolean validateEnteredDate(String date )
	{
		Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
		Matcher matcher = pattern.matcher(date);
		boolean validDate = matcher.matches();
		if(validDate) return true;
		return false;
	}
	
	public void validateOncologist( String oncologist, String oncologistList, String bookedOncologistsList )
	{
		
	}


    public boolean insertTestTime(String time )
	{
		boolean timeIsValid = validateTime(time);
		if(timeIsValid){
			this.time = time;
			return true;
		}
		return false;
	}
	
	public boolean validateTime( String time )
	{
		Pattern pattern = Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9]");
		Matcher matcher = pattern.matcher(time);
		boolean validTime = matcher.matches();
		if(validTime) return true;
		return false;
	}
	
	public boolean insertTestDate( String date )
	{
		boolean dateIsValid = validateEnteredDate(date);
		if(dateIsValid){
			this.oldTestDate = date;
			return true;
		}
		return false;
	}
	
	public boolean validateTestDescription(String description )
	{
		return description.length()!=0;
	}
	
	public int insertUserName( String userName )
	{
		boolean userNameIsValid = validateUserName(userName);
		if(userNameIsValid){
			boolean canBeUsed = UserDAO.checkUserNameAvailability(userName);
			if(canBeUsed){
				this.username = userName;
				return 0;
			}
			return 2;
		}
		return 1;
	}
	
	public boolean validateUserName( String userName )
	{
		Pattern pattern = Pattern.compile("^(?=.{5,16}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$");
		Matcher matcher = pattern.matcher(userName);
		boolean validUsername = matcher.matches();
		if(validUsername) return true;
		return false;
	}
	
	public boolean insertPassword( String password )
	{
		boolean passIsValid = validatePassword(password);
		if(passIsValid){
			this.password = password;
			return true;
		}
		return false;
	}
	
	public boolean validatePassword( String password )
	{
			Pattern pattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,15})");
		Matcher matcher = pattern.matcher(password);
		boolean validPassword = matcher.matches();
		if(validPassword) return true;
		return false;
	}
	
	public boolean insertRole( String role )
	{
		boolean roleIsValid = validateRole(role);
		if(roleIsValid){
			UserDAO.createUser(this.username, this.password, role);
			return true;
		}
		return false;
	}
	
	public boolean validateRole( String role )
	{
		List<String> validRoles = Arrays.asList("doctor", "receptionist", "administrative officer", "surgeon");
		return validRoles.contains(role.toLowerCase());
	}
	
	public void selectReport( String report )
	{
		
	}
	public  boolean selectReportIdentifierforViewing(String identifier ){
        boolean isvalid = this.patientReportIdentifiersAndReports.containsKey(identifier.toLowerCase());
        if (isvalid && !identifier.toLowerCase().equals("quit")){
            System.out.println();
            System.out.println("-------------------------------------------");
            System.out.println();
            System.out.println(this.patientReportIdentifiersAndReports.get(identifier.toLowerCase()));
            System.out.println();
            System.out.println("-------------------------------------------");
            System.out.println();
            return true;
        }
        else if (identifier.toLowerCase().equals("quit")){
            return true;
        }
        return false;
    }

	public boolean selectReportIdentifier(String identifier )
	{

        boolean isvalid = this.patientReportIdentifiersAndReports.containsKey(identifier.toLowerCase());
        if (isvalid && !identifier.toLowerCase().equals("quit")){
            System.out.println();
            System.out.println("-------------------------------------------");
            System.out.println("Report is being printed. Please wait . . .");
            System.out.println("-------------------------------------------");
            System.out.println();
            return true;
        }
        else if (identifier.toLowerCase().equals("quit")){
            return true;
        }
		return false;
	}
	
	public void validateReportIdentifier( String identifier )
	{
		
	}
	
	public boolean defineSurgeryTeam(String surgery_id )
	{
		surgeryId = surgery_id;
		boolean validSurgeryID = SurgeryDAO.checkIfSurgeryExists(surgery_id);
		return validSurgeryID;
	}
	
	public int insertTeamMembers(String team )
	{
		boolean validInput = validateTeamMembersInput(team);
		if (!validInput)
			return 1;
		String[] members = team.split(",");
		for (String m: members) {
			if(!oncologistDAO.getOncologistByID(m))
				return 2;
		}
		SurgeryDAO.addSurgeryTeam(surgeryId, team);
		return 0;
	}
	
	public boolean validateTeamMembersInput(String team )
	{
		String[] members = team.split(",");
		for (String m: members) {
			if (!validateID(m))
				return false;
		}
		return true;
	}
	
	public boolean insertMedicineCompany(String company )
	{
		boolean medicineCompanyIsValid = validateMedicineCompanyName(name);
		if (medicineCompanyIsValid) {
			this.medicineCompany = company;
			return true;
		}
		return false;
	}
	
	public boolean validateMedicineCompanyName(String company )
	{

		return true;
	}
	
	public int insertMedicineCode(String medicine_code )
	{
		boolean validMedicineCode = validateMedicineCode(medicine_code);
		if (validMedicineCode) {
			boolean alreadyExists = MedicineDAO.getInfo(medicine_code);
			if (alreadyExists)
				return 2;
			else {
				MedicineDAO.addMedicine(medicineName, medicineCompany, medicine_code);
				return 0;
			}
		}
		return 1;

	}
	
	public boolean validateMedicineCode(String medicine_code )
	{
		try {
			Integer.valueOf(medicine_code);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean inserTestType( String type )
	{
		boolean typeIsValid = validateTestType(type);
		if(typeIsValid){
			this.testType = type;
			return true;
		}
		return false;
	}
	
	public boolean validateTestType( String type )
	{
		List<String> validTypes = Arrays.asList("X-Ray", "PET", "TC", "MRI", "blood");
		return validTypes.contains(type);
	}
	
	public void timeInAvailableSlotList( String time, String availableSlotList )
	{
		
	}
	
	public void validateVisitID( String visit_id )
	{
		
	}
	
	public void validateSurgeonID( String surgeon_id )
	{
		
	}
	
	public void surgeonIsAvailable( String surgeon_id, String bookedSurgeonList )
	{
		
	}
	
	public String addSurgeon( )
	{
		return "Started account registration process";
	}
    public int insertSurgeonTypeforSurgery(String type )
    {

        boolean validSurgeonType = validateSurgeonType(type);
        if (validSurgeonType) {
            boolean existingType = SurgeonDAO.hospitalHasThisTypeOfSurgeon(type);
            if(existingType){
                this.surgeonType = type;
                return 0;
            }
            return 1;
        }
        return 2;
    }

	public boolean insertSurgeonType(String type )
	{
		boolean validSurgeonType = validateSurgeonType(type);
		if (validSurgeonType) {
            this.type = type;
			SurgeonDAO.addSurgeon(name, surname, surgeonId, type);
			return true;
		}
		return false;
	}
	
	public boolean validateSurgeonType( String type )
	{
		try {
			SurgeonType.valueOf(type.toUpperCase());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public String addOncologist( )
	{
		return "Starting oncologist adding process";
	}
	
	public boolean insertOncologistType(String type )
	{
		this.type = type;
		boolean validOncologistType = validateOncologistType(type);
		if (validOncologistType)
			return true;
		return false;
	}
	
	public boolean validateOncologistType(String type )
	{
		try {
			OncologistType.valueOf(type.toUpperCase());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean insertLevelOfCareer(String level )
	{
		this.levelOfCareer = level;
		boolean validLOC = validateLevelOfCareer(level);
		if (validLOC) {
			oncologistDAO.addOncologist(name, surname, oncologistId, type, level);
			return true;
		}
		return false;
	}
	
	public boolean validateLevelOfCareer(String level )
	{
		// Medical Student, Physician Assistant, Fellow, Specialist
		if (level.toLowerCase().equals("medical student") || level.toLowerCase().equals("physical assistant") || level.toLowerCase().equals("fellow") || level.toLowerCase().equals("specialist"))
			return true;
		return false;
	}
	
	public boolean dletePatientFolder(String patient_id )
	{
        List<String> folder = PatientFolderDAO.getFolderByPatientID(patient_id);
        if(folder.size()!=0) {
            HashMap<String,String> deletionInfo = PatientFolderDAO.dropPatientFolder(patient_id);

            //making booked test slots available
            if(deletionInfo.get("testidlist").length()!=0){
                String[] test_ids = deletionInfo.get("testidlist").split(",");
                for(String te_id : test_ids){
                    TestDAO.dropPatientTests(Integer.valueOf(te_id));
                }
            }
            System.out.println("Patient booked test slots freed");

            //delete patient associated rows from therapies
            if(deletionInfo.get("therapyidlist").length()!=0){
                String[] therapy_ids = deletionInfo.get("therapyidlist").split(",");
                for(String the_id : therapy_ids){
                    TherapyDAO.dropPatientTherapies(the_id);
                }
            }
            System.out.println("Patient therapies removed from database");

            //delete patient associated rows from surgeries
            if(deletionInfo.get("surgeryidlist").length()!=0){
                String[] surgery_ids = deletionInfo.get("surgeryidlist").split(",");
                for(String sur_id : surgery_ids){
                    SurgeryDAO.dropPatientSurgeries(sur_id);
                }
            }
            System.out.println("Patient surgeries removed from database");


            //delete patient associated rows from visits
            visitDAO.dropPatientVisits(patient_id);
            System.out.println("Patient visits removed from database");
            System.out.println("Patient folder deleted from system!");
            System.out.println("-----------------------------------");
            System.out.println();
            return true;
        }
        return false;
	}
	
	public ArrayList<Surgeon> viewSurgeons( )
	{
		return SurgeonDAO.getAllSurgeons();
	}
	
	public boolean assignOncologist(String patient_folder_id )
	{
		patienFolderId = patient_folder_id;
		boolean validateID = PatientFolderDAO.getFolderByPatientID(patient_folder_id).size() > 0;
		if (validateID)
			return true;
		return false;
	}
	
	public ArrayList<Oncologist> viewOncologists( )
	{

		return oncologistDAO.getOncologists();
	}
	
	public boolean setFirstVisitDate(String patient_folder_id)
	{
		patienFolderId = patient_folder_id;
		boolean validateID = PatientFolderDAO.getFolderByPatientID(patient_folder_id).size() > 0;
		return validateID;
	}
	
	public boolean insertDate(String date )
	{
		boolean validDate = validateEnteredDate(date);
		if (!validDate)
			return false;
		PatientFolderDAO.addFirstVisitDate(patienFolderId, date);
		return true;
	}
	
	public ArrayList<Patient> viewPatients( )
	{
		return PatientFolderDAO.getAllPatients();
	}
	
	public int insertMedicineList(String medicine_list )
	{
		medicineList = medicine_list;
		ArrayList<Medicine> getAllMedicines = MedicineDAO.getAllNames();
		String[] enteredMedicines = medicine_list.split(",");
		this.enteredMedicinesCount = enteredMedicines.length;
		boolean medicineExistsInList;
		for (String s: enteredMedicines) {
			medicineExistsInList = false;
			for (Medicine m : getAllMedicines) {
				if (m.getCode().equals(s))
					medicineExistsInList = true;
			}
			if (medicineExistsInList == false)
				return 1;
		}
		Set<String> setOfMedicines = new HashSet<String>(Arrays.asList(enteredMedicines));
		if (setOfMedicines.size() != enteredMedicines.length)
			return 2;
		return 0;
	}
	
	public void validateMedicineList( String medicine_list )
	{
		
	}
	
	public int insertPosologyList(String posology_list )
	{
		posologyList = posology_list;
		String[] posologies = posology_list.split(",");
		try {
			for (String s: posologies) {
				Float p = Float.valueOf(s);
			}
			if (posologies.length != enteredMedicinesCount)
				return 1;
		} catch (Exception e) {
			return 2;
		}
		return 0;
	}
	
	public void validatePosologyList( float posology_list )
	{
		
	}
	
	public void flatenUniqueFilter( String membersListList )
	{
		
	}
	
	public void filterSpecialistFellows1( String oncologistList )
	{
		
	}
	
	public void filterSpecialistFellows( String bookedOncologistsList )
	{
		
	}
	
	public void insertTherapyDays1( String days )
	{
		
	}


	public int insertIDSurgeon(String s) {
		this.surgeonId = s;
		boolean validId = validateID(s);
		if (validId) {
			boolean existingSurgeonId = SurgeonDAO.getSurgeonByID(s);
			if (existingSurgeonId) {
				return 2;
			}
			return 0;
		}
		return 1;
	}

	public int insertTestTimeInt(String time)
    {
        boolean timeIsValid = validateTime(time);
        if(timeIsValid){
            this.bookingTestTime = time;
            if(this.availableTestSlots.contains(time)){
                this.bookingTestTime = time;
                String addForDB = ":00.000000";
                int testID = TestDAO.addSlotBooking(this.bookingTestTime+addForDB, this.testDate, this.testType);
                visitDAO.addTestReference(testID, this.visitID);
                List<String> nameOfPatient = PatientFolderDAO.addTestReference(testID, this.visitID);
				System.out.println("Test booked successfully!");
				System.out.println("Patient: "+nameOfPatient.get(1)+", "+nameOfPatient.get(0));
				System.out.println("Type: "+this.testType);
				System.out.println("Date: "+this.testDate);
				System.out.println("Time: "+this.bookingTestTime);
				System.out.println("------------------------------------------------------");
                return 0;
            }
            return 1;
        }
		return 2;
	}

	public int insertIDOncologist(String s) {
		this.oncologistId = s;
		boolean validId = validateID(s);
		if (validId) {
			boolean existingOncologistId = oncologistDAO.getOncologistByID(s);
			if (existingOncologistId) {
				return 2;
			}
			return 0;
		}
		return 1;
	}

	public int insertStartDate(String s, boolean fetchFromDb) {
		boolean validDate = validateEnteredDate(s);

		if (validDate) {
			if (fetchFromDb) {
				String therapyIds = PatientFolderDAO.getTherapyIDs(patienFolderId);
				String[] therapies = therapyIds.split(",");
				ArrayList<ArrayList<Date>> startAndEndTimeLists = TherapyDAO.getTherapyDatesOfPatient(therapies);
				this.startDates = startAndEndTimeLists.get(0);
				this.endDates = startAndEndTimeLists.get(1);
			}

			String[] parts = s.split("-");
			Integer years = Integer.valueOf(parts[0]);
			Integer months = Integer.valueOf(parts[1]);
			Integer days = Integer.valueOf(parts[2]);


			Date date = new Date(years, months, days);
			if(fetchFromDb)this.therapyStartDate = new Date(years, months, days);



			for (int i = 0; i < startDates.size(); i++) {
				if (date.compareTo(startDates.get(i)) >= 0 && date.compareTo(endDates.get(i)) <= 0) {
					return 2;
				}
			}
			return 0;
		}
		return 1;
	}

	public int insertEndDate(String s) {
		int result = insertStartDate(s,false);
		String[] info1 = s.split("-");
		int year1 = Integer.valueOf(info1[0]);
		int month1 = Integer.valueOf(info1[1]);
		int day1 = Integer.valueOf(info1[2]);
		Date endd = new Date(year1, month1, day1);

		if(endd.compareTo(therapyStartDate)<0)return 3;
		if (result != 0)
			return result;
		else {
		    String startDate = therapyStartDate.toString();
		    String[] info = startDate.split("-");
		    int year = Integer.valueOf(info[0])-3800;
            int month = Integer.valueOf(info[1])-2;
            int day = Integer.valueOf(info[2]);
			TherapyDAO.insertTherapy(visitID, patienFolderId, therapyType, medicineList, posologyList, new Date(year, month, day), s);
			return 0;
		}
	}

	public int selectVisitDay(String day) {
		if(!validateEnteredDate(day)){
			return 2;
		}
		HashMap<String,Integer> oncolosVisitsOnDay = new HashMap<>();
		List<String> bookedoncosOnDay = visitDAO.getOncologistOnDay(day);
		for (String booked : bookedoncosOnDay){
			int counter = 0;
			for (int i = 0; i < bookedoncosOnDay.size();i++){
				if (bookedoncosOnDay.get(i).equals(booked))counter++;
			}
			oncolosVisitsOnDay.put(booked, counter);
		}
		ArrayList<Oncologist> alloncos = oncologistDAO.getOncologists();
		List<String> freeoncos = new ArrayList<>();
		for (Oncologist onco : alloncos){
			String name = onco.getName();
			if(oncolosVisitsOnDay.containsKey(name)){
				if(oncolosVisitsOnDay.get(name)<10){
					freeoncos.add(onco.getProfessionalID());
				}
			}else{
				freeoncos.add(onco.getProfessionalID());
			}
		}
		if(freeoncos.size()!=0){
			visitDay = day;
			freeOncologistOnDay = freeoncos;
			System.out.println(freeoncos);
			return 0;
		}

		return 1;
	}
}