import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @(#) SurgeryDAO.java
 */

public class SurgeryDAO
{
	static int SURGERY_ID = 0;

	public static HashMap<String,String> getBookedSurgeonsOnDay(String day )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		HashMap<String,String> output = new HashMap<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT surgeonID, datetime FROM surgery WHERE datetime LIKE '%"+day+"%';");
			while(result.next()){
				output.put(result.getString(1), result.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public void insertSurgery( String surgeon_id )
	{
		
	}
	
	public void insertSurgery( String surgeon_id, String patient_id )
	{
		
	}
	
	public static String addSurgery( String surgeon_id, String day)
	{
		Connection con = null;
		Statement stmt = null;
		String new_ID = "0";
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			new_ID = Integer.toString(SURGERY_ID);
			stmt = con.createStatement();
			stmt.executeUpdate("INSERT INTO surgery VALUES ('"+(SURGERY_ID++)+"','"+surgeon_id+"','"+day+"','');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return new_ID;
	}
	
	public static boolean checkIfSurgeryExists(String surgery_id )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM surgery WHERE surgeryID = '"+surgery_id+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static void addSurgeryTeam( String surgery_id, String team )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE surgery SET oncologistsidlist = '"+team+"' WHERE surgeryID = '"+surgery_id+"';");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static void dropPatientSurgeries( String id )
	{
		Connection con = null;
		Statement stmt = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate(
					"DELETE FROM surgery WHERE surgeryID = '"+id+"';");
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public void getSurgeryMemberListList( )
	{
		
	}
	
	
}
