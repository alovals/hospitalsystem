import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class DatabaseManager {

	public static void initializeDatabase(){
		Connection con = null;
		Statement stmt = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("CREATE TABLE patientfolder (folderID varchar(20) NOT NULL, name varchar(50) NOT NULL, surname varchar(50) NOT NULL, patientID varchar(20) NOT NULL, oncologistID varchar(20), birthdate DATE NOT NULL, code varchar(70) NOT NULL, firstvisitdate varchar(20), anamnesis varchar(4000), testidlist varchar(4000), therapyidlist varchar(4000), surgeryidlist varchar(4000), PRIMARY KEY (folderID));");
			stmt.executeUpdate("CREATE TABLE medicine (medicineID varchar(30) NOT NULL, name varchar(30) NOT NULL, company varchar(40) NOT NULL, PRIMARY KEY (medicineID));");
			stmt.executeUpdate("CREATE TABLE oncologist (professionalID varchar(20) NOT NULL, name varchar(50) NOT NULL, surname varchar(50) NOT NULL, type varchar(50) NOT NULL, level varchar(25) NOT NULL, PRIMARY KEY (professionalID));");
			stmt.executeUpdate("CREATE TABLE surgeon (professionalID varchar(20) NOT NULL, name varchar(50) NOT NULL, surname varchar(50) NOT NULL, type varchar(50) NOT NULL, PRIMARY KEY (professionalID));");
			stmt.executeUpdate("CREATE TABLE surgery (surgeryID varchar(20) NOT NULL, surgeonID varchar(20) NOT NULL, datetime TIMESTAMP NOT NULL, oncologistsidlist varchar(4000), PRIMARY KEY (surgeryID));");
			stmt.executeUpdate("CREATE TABLE therapy (therapyID varchar(20) NOT NULL, type varchar(20) NOT NULL, medicineidlist varchar(4000), posologylist varchar(4000), startdate DATE NOT NULL, enddate DATE NOT NULL, schedulelist varchar(4000), PRIMARY KEY (therapyID));");
			stmt.executeUpdate("CREATE TABLE visit (visitID varchar(20) NOT NULL, date varchar(20) NOT NULL, patientID varchar(20) NOT NULL, oncologistID varchar(20) NOT NULL, therapyidlist varchar(4000), testidlist varchar(4000), surgeryidlist varchar(4000), report varchar(1024) NOT NULL, PRIMARY KEY (visitID));");
			stmt.executeUpdate("CREATE TABLE test (testID INT NOT NULL, type varchar(20) NOT NULL, testdate DATE NOT NULL, testtime TIME NOT NULL, availability BOOLEAN NOT NULL, PRIMARY KEY (testID));");
			stmt.executeUpdate("CREATE TABLE user (username varchar(30) NOT NULL, password varchar(30) NOT NULL, role varchar(30) NOT NULL, PRIMARY KEY (username));");

			/*
			//insert patient folder
			stmt.executeUpdate("INSERT INTO patientfolder VALUES ('1','Alo','Vals','69','','1995-05-31','Vals69','','','','','');");
			stmt.executeUpdate("INSERT INTO patientfolder VALUES ('2','Joosep','Tenn','1337','','1994-09-18','Tenn1337','','','','','');");
			stmt.executeUpdate("INSERT INTO patientfolder VALUES ('3','Unnamed','She','23','','1997-02-14','She23" +
					"','','','','','');");

			//insert oncologist
			stmt.executeUpdate("INSERT INTO oncologist VALUES ('666','Doctor','Vassiljev','GYNECOLOGIC','Specialist');");

			//insert visit
			stmt.executeUpdate("INSERT INTO visit VALUES ('1','2018-11-17','69','666','','','','Lastly, the doctor writes a report about the first visit and she books the follow-up visit (with an available\n" +
					"oncologist). The doctor can specify a day and get the availability of oncologists for that day, and can\n" +
					"book a free oncologist. During the follow-up visit, the doctor evaluates the conditions of the patient\n" +
					"and the info collected in the patient folder and after the evaluation follows the same procedure as in\n" +
					"the first visit: (i) she asks for more clinical tests and/or (ii) she chooses a therapeutic treatment and/or\n" +
					"(iii) she books a surgery. Lastly, the doctor writes a report about the follow-up visit and she books the\n" +
					"next follow-up visit. \n');");

            //insert medicine
            stmt.executeUpdate("INSERT INTO medicine VALUES ('0','Aspirin','TharmaComp');");

            //insert therapy
            stmt.executeUpdate("INSERT INTO therapy VALUES ('0','day','0','5','"+new Date(2018-1900, 10, 12)+"','"+new Date(2018-1900, 10, 26)+"','');");

            //insert surgeon
            stmt.executeUpdate("INSERT INTO surgeon VALUES ('0','Talisman','Viper','oral');");

            //add acoount
			stmt.executeUpdate("INSERT INTO user VALUES ('DrAlo', 'Alovals123', 'doctor')");

			//add surgery
			//stmt.executeUpdate("INSERT INTO surgery VALUES ('0', '0', '2018-11-17 20:00:00','')");
			*/
            //available slots to test table
			//imaging tests
			int id = 1;
			int hour = 9;
			int minutes = 0;
			boolean hourPassed = false;
			List<String> validTypes = Arrays.asList("X-Ray", "PET", "TC", "MRI");
			for(String type: validTypes) {
				for(int i = 1; i < 6; i++){
					stmt.executeUpdate("INSERT INTO test VALUES ('"+id+"','"+type+"','2018-11-17','"+hour+":"+minutes+":00.000000','true');");
					id++;
					if(hourPassed){
						hour++;
						minutes = 0;
						hourPassed = false;
					}else{
						minutes = 30;
						hourPassed = true;
					}
				}
			}
			//blood tests
			int hourb = 9;
			int minutesb = 0;
			for(int i = 1; i < 13; i++){
				stmt.executeUpdate("INSERT INTO test VALUES ('"+id+"','blood','2018-11-17','"+hourb+":"+minutesb+":00.000000','true');");
				id++;
				if(minutesb==50){
					hourb++;
					minutesb = 0;
				}else{
					minutesb += 10;
				}
			}

			System.out.println("Database initialized successfully");
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	public static void resetDatabase(){
		Connection con = null;
		Statement stmt = null;

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("DROP TABLE patientfolder;");
			stmt.executeUpdate("DROP TABLE medicine;");
			stmt.executeUpdate("DROP TABLE oncologist;");
			stmt.executeUpdate("DROP TABLE surgeon;");
			stmt.executeUpdate("DROP TABLE surgery;");
			stmt.executeUpdate("DROP TABLE therapy;");
			stmt.executeUpdate("DROP TABLE visit;");
			stmt.executeUpdate("DROP TABLE test;");
			stmt.executeUpdate("DROP TABLE user;");
			System.out.println("Database reset successfully");
		}  catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}



}
