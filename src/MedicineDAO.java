import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @(#) MedicineDAO.java
 */

public class MedicineDAO
{
	public static void addMedicine( String name, String company, String medicine_code )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO medicine VALUES ('"+medicine_code+"','"+name+"','"+company+"');");
			con.commit();
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static ArrayList<Medicine> getAllNames( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Medicine> output = new ArrayList<Medicine>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM medicine;");
			while(result.next()){
				Medicine m = new Medicine();
				m.setCode(result.getString("medicineID"));
				m.setName(result.getString("name"));
				m.setPharmaceuticalCompany(result.getString("company"));
				output.add(m);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean getInfo( String medicine )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM medicine WHERE medicineID = '"+medicine+"';");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	
}
