import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) TestDAO.java
 */

public class TestDAO
{
	public static List<String> getAvailableTestSlots(String date, String test_type )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean isAvailable = true;
		List<String> output = new ArrayList<>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT testtime FROM test WHERE testdate = '"+date+"' AND type = '"+test_type+"' AND availability = '"+isAvailable+"';");
			while(result.next()){
				output.add(result.getString(1));
			}
			return output;
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static int addSlotBooking( String time, String date, String type)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		int test_id = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE test SET availability = 'false' WHERE testtime = '"+time+"' AND testdate = '"+date+"' AND type = '"+type+"';");
			con.commit();
			result = stmt.executeQuery("SELECT testID FROM test WHERE testtime = '"+time+"' AND testdate = '"+date+"' AND type = '"+type+"' AND availability = 'false';");
			while(result.next()){
				test_id = result.getInt(1);
			}
		}catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return test_id;
	}
	
	public static void dropPatientTests( int id )
	{
		Connection con = null;
		Statement stmt = null;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/universitydb", "SA", "");
			stmt = con.createStatement();
			stmt.executeUpdate(
					"UPDATE test SET availability = 'true' WHERE testID = '"+id+"';");
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	
}
