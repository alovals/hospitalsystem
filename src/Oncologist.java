/**
 * @(#) Oncologist.java
 */

public class Oncologist
{
	private String name;
	
	private String surname;
	
	private String professionalID;
	
	private OncologistType oncologistType;
	
	private String surgery;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getProfessionalID() {
		return professionalID;
	}

	public void setProfessionalID(String professionalID) {
		this.professionalID = professionalID;
	}

	public OncologistType getOncologistType() {
		return oncologistType;
	}

	public void setOncologistType(OncologistType oncologistType) {
		this.oncologistType = oncologistType;
	}

	public String getSurgery() {
		return surgery;
	}

	public void setSurgery(String surgery) {
		this.surgery = surgery;
	}

	@Override
	public String toString() {
		return "Oncologist{" +
				"name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", professionalID='" + professionalID + '\'' +
				", oncologistType=" + oncologistType +
				'}';
	}
}
