import java.sql.Date;

/**
 * @(#) Therapy.java
 */

public class Therapy
{
	private String medicine;

	private String posology;

	private String schedule;

	private String type;
	
	private Date startOfTherapy;
	
	private Date endOfTherapy;

	public String getMedicine() {
		return medicine;
	}

	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}

	public String getPosology() {
		return posology;
	}

	public void setPosology(String posology) {
		this.posology = posology;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getStartOfTherapy() {
		return startOfTherapy;
	}

	public void setStartOfTherapy(Date startOfTherapy) {
		this.startOfTherapy = startOfTherapy;
	}

	public Date getEndOfTherapy() {
		return endOfTherapy;
	}

	public void setEndOfTherapy(Date endOfTherapy) {
		this.endOfTherapy = endOfTherapy;
	}
}
